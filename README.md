# BuildAModule Front End Development Course
https://buildamodule.com/collection/front-end-development

### Tools
* MAC built in screen capture tool
  * Cmd-Shift-3 - take screen-shot of entire screen and copy .png to Desktop
  * Cmd-Shift-4 - select and copy to Desktop
  * Cmd-Ctl-Shift-4 - select and copy to Clipboard  
* Use screen capture tool to measure width and height of elements, margins, 
  spacing etc.
* SIP Color-picker for MAC
* Use SIP to select colors on mockup and convert to hex code.

* This course goes through the steps to convert a Design mockup to HTML and CSS
### Steps
* **Review Mockup**
  * Ideally use PhotoShop Mockup
  * When looking at a mockup, go through the exercise of putting
    boxes around sections of the mockup - this is a good way to visualize
    the various elements that will be needed.
  * Try to break down the boxes into their smallest component
  * This is called an _atomic or component based approach_
* **Write HTML (with no styling)**
* Style Layout
* Style topography, colors, spacing
* For each component:
  * Identify header(s)
  * Style headers with font and typography that matches design mockup
  * Use screen capture tool for measurement and SIP for color matching
  * Use SMACSS style class names for styling
* For each component:
  * Style remaining text (paragraphs etc.) - concentrating on typography

## Chapters

### 1. Welcome to Front End Development
* Based on mockup,  I wrote the HTML (index.html) for the page (no styling)
* As I progress through each chapter, classes will be added to HTML elements  
  so they can be styled.
* Seems to me too that we should do the layout of the page before styling the
  the individual components  

### 2. HTML and CSS Basics
* Review HTML tags and CSS selectors
* Go through steps for writing HTML/CSS for Headings on Home page mockup
  * Headings: Qualities, Written By Our Team, Contact Us
  * Use ```<h2>``` , ```<h3>``` etc tags for headings
  
### 3. HTML and CSS Basics, Part 2  
* Review CSS classes
* Begin working on page layout
  * Visualize drawing a box around each section of the page
  * Get a visual cue from the Headings that are on the page
* Start with implementing Headings on the page
* Add ```<section>``` tag around each Heading
* Style "Written By Our Team" section
* Style "More than just a company" section (Slider section)
* Discuss Cascade, Specificity, order of styles
* Use stylesheet styles.css instead of HTML ```<style>``` tag 
* Use _Semantic class names_ to target elements - more flexible than just using
element selectors (h2 etc.)

### 4. How to Orgazine CSS and use SMACSS
* CSS coding (class names etc.) will follow SMACSS standard
* In SMACSS, website sections/areas are referred to as modules or components
* Rename classes following SMACSS
* Continue tackling typography
* Work on Headings in Qualities "Hero" section
  * PASSION, SPEED etc.
* Work on remainder of Headings 
  * "The top 10 things you need to know and "Questions ..."
* Next, tackle the rest of text on the page (paragraphs etc.)

### 5. CSS Resets, Normalize.css and Package Management with Bower
* Add ```body```  tog in CSS to handle common typography elements
  (font-family, font-weight etc.)
* Add ```h1-h3``` and ```p``` tags in CSS to override browser defaults
* Discuss CSS Resets
* CSS Resets are used to handle browser styling differences:
  * Eliminates browser CSS defaults
  * Avoid the temptation of using elements just for looks
  * Cross-browser consistency
* Biggest issue for different browsers is that they use different
  margin, padding and border values.
* Preference is to use _Normalize.css_ 
* Discuss the need for _Package Managers_ 
  * Package managers handle installing, updating and handling dependencies
    of front-end third party code and libraries like normalize.css.
  * This saves a lot a manual work when working with third party code.  
* **_Bower_** is used for front-end package management 
  * Appears though from the Bower website that Webpack/Yarn or
    Parcel has superseded Bower as the preferred tool.
* Install bower
  * ```npm install -g bower```
* Install normalize.css
  * cd to project directory 05-css-resets
  * ```bower install normalize.css```  
* Create bower.json file
  * ```bower init```  
  * This file will contain our project packages/libraries (and their dependencies)
* Use ```bower update```  to update packages

#### 6. How CSS Units Work
* Discuss absolute and relative units
* Pixels, ```px``` are absolute units. ```em```, ```rem```, percentages are 
  relative units
* Use CSS pre-processor to handle unit conversions
* Discuss rem and the fact that all browsers may not support it
* Dovetail into using ```canisuse.com```  to check browser support for
  particular CSS/HTML features, elements etc.
* Can at times use **_JavaScript polyfills_** to enable older browsers to use new
  CSS/HTML features, properties, elements etc.  
  
#### 7. CSS Preprocessing and SASS
* Why use a CSS pre-processor like SASS ?
  * Reduce repetition by using variables for colors etc.
  * **_Allows selector nesting to improve readability_**
  * Add meaning to values
  * Perform calculations and logic
* SASS is more commonly used than LESS or Stylus
* Rename css/styles.css to css/styles.scss
* Generate styles.css
```
cd css
node-sass styles.scss styles.css
```
* SASS allows ```/* */``` and ```//```  in ```.scss``` file for comments
* Comments are stripped out of generated ```.css``` file
* Nest all component selectors
* Nesting visually organizes the selectors

#### 8. Project Organization With SASS and Using Source Maps
* Re-organize ```.sccs``` files in ```sass/``` folder and ```.css``` file in ```css/``` folder
* Create sass/components/ directory and add sass "partials" files
* Use this command to "watch" sass files
```
 node-sass --watch 08-project-organization-with-sass/sass/ \\ 
 08-project-organization-with-sass/sass/components \\
 --output 08-project-organization-with-sass/css/ --source-map true --source-map-contents sass
```
* ```css/styles.css.map``` is used to css to imported files
* This file is called a source map and as long it has browser support, always
one to see which .scss file provides a partiular piece of CSS.
* Look into using globbing support for node-sass

#### 9. Using Bundler to Manage SASS Extensions
* Skip Bundler sections since we're not using Ruby/gems
* Use this command to "watch" sass files
```
node-sass --watch ch09/sass/ 09/sass/components/ --output ch09/css/ --source-map true --source-map-contents sass
```
* Work on templating footer section
* Suggestion: For web accessibility and screen readers use,
  don't use ``<ul>`` ```<li>``` list elements for navigation.
* Style footer primary navigation link

### 10. How to use SASS Functions
* Create ```_functions.scss``` file
* Create function for converting ```px``` to ```em```
```
@function px2em($size-in-px) {
  @return ($size-in-px/$default-font-size) + em;
}
```
* Create function for converting ```px``` to ```rem```
* Go through component CSS and convert px measurement to ```em``` or ```rem```

### 11 Styling Typography With CSS
* Reference: Development/Part 1 Steps/22.using-sass-bundler-and-sass-globbing.step
* Continue footer styling
* Style middle footer section and primary/secondary nav links
* Style header navigation links
* Style buttons
* Create ```_button.scss``` component
* Style "Learn More" button first
  * Use ```lighten()``` and ```darken()``` Sass functions on button border color
* Style "Read On buttons"
* Style Phone contact button 
* At this point most of the site Topography is done - big achievement 'cause
  typography is often the hardest part in converting a design to a template.
  
### 12. Working With Icon Fonts
* Using Icon fonts as opposed to images for icons is more flexible
* These next set of instructions deviate from video as the video uses an older
  version (4.3.0) of fontawesome. We are using version 5.4.1
* Both the instructions here and in the video use the "CSS Pseudo-elements"
  way ```(https://fontawesome.com/how-to-use/on-the-web/advanced/css-pseudo-elements)```    
  to implement the icon fonts.  
* Doing it this way gives complete control of how the icons look using css    
  (font-size, color etc.) and doesn't require the use of the entire fontawesome
  framework.  
* Not using the entire framework cuts down on the amount of css.
* Download fontawesome using bower
```bash
bower install fontawesome --save
```
* Copy ```@font-face ``` rules from 
  ```bower_components/fontawesome/web-fonts-with-css/css/solid.css```   
   and  
  ```bower_components/fontawesome/web-fonts-with-css/css/brand.css``` to  
  ```_base.scss```
* Create ```_play.scss``` file to experiment with using icon fonts
* Go to ```https://fontawesome.com/cheatsheet``` to get character codes for icons
* Add icon box styling in ```_play.scss``` .Use CSS trick to create circle. 
* Update ```_icon_box.scss``` and ```_site_header.scss``` with icon styling
* Discuss how to pull in particular fontawesome scss partials to add icons
  using fontawesome classes ```(fa, fa-heart)``` etc.
* Discuss using Fontastic vs fontawesome
* Created account (raynaldmo@gmail.com) on ```http://fontastic.me/```
* With Fontastic you can create customized font files ```(.woff, .eot)``` 
  with just the icons that you need. This cuts down the size of these files 
  quite a bit as it eliminates un-used/un-needed icons.
* Third-party text fonts.  
* Go to https://www.fontsquirrel.com/ and download "Alex Brush Regular" font
  * font and documentation is in 12-working-with-icon-fonts/fontsquirrel/  
* Discuss Google Fonts
* Pro/Cons using FontSquirrel vs Google Fonts
* Style Header Account link and add icon
* Style social icons links in footer
* Circles and Circle Icons
  * ```.site_footer-social_nav``` class is a good example of a robust way to
    create a circle icon
    
### 13. Customizing Forms
**_Note:_**  
Starting at this point we'll stop copying code to different directories and   
use git if we need to view work that was done in a specific chapter.  
* Go to ```file:///Users/raynald/PhpStormProjects/front-end-development/index.html```
  to access site
* Begin styling Contact form
* Style Submit button
* Style Comment textarea
* Style Select list
  * Browser differences make styling ```<select>``` lists difficult.
  * Use ```selectize.js``` (https://selectize.github.io/selectize.js/) to  
    get consistent select list styling across different browsers.
  * ```bower install selectize --save```
  * In ```index.html``` add ```selectize.js``` and related scripts
  * To handle IE9 lack of support for placeholder attritube use:  
    Placeholder.js JavaScript polyfill (https://jamesallardice.github.io/Placeholders.js/)
  * Download using bower:   ```bower install placeholders --save```
  * Finish styling select box by over-riding styles added by ```selectize.js```      
* Style email and name text inputs
* In ```_contact_form.scss``` add ```%gray-input placeholder``` selector and  
  use ```@extend``` to apply to all input selectors
  * Placeholder selectors are ```%-contact_form---gray_input_input``` and  
    ```%-contact_form---gray_input_input```  
* Style radio buttons
  * CSS deviates slightly from video
* Style contact description text
  * CSS deviates slightly from video 
* Style search form in header
  * Create ```_search_form.scss``` component 
  * Note that the ```<select>``` element of the form will be initially styled   
    by ```selectize.js```.
  * Hide 'caret' icon
  * Add Font Awesome _sort_ icon
  * Add search icon - Note for the this to work ```<button>``` element must be used
  * CSS styles deviate slightly from video
* Use Fontastic to create icons for search form and user account

### 14. Working With Responsive Images and Breakpoints
* Copy logo.png and logo@2x.png from 
  ```Development/Development/Part 1 Steps/44.completing-responsive-layout.step```  
  to images/ since I don't have PhotoShop  
* Images have a wide variety of resolutions
  * Devices/screens differ in their ability to display high resolution images. 
  * It's a waste of bandwidth to serve high resolution images to devices/screens  
    that can't display them.
* Solutions
  * Always serve high resolution images or whatever images are available
  * Use ```retina.js``` to decide which image to serve       
* Use ```retina.js``` to download high-resolution images to devices/screens that can   
  display them, otherwise download lower resolution image. This saves bandwidth.    
* Download ```retina.js```  
```bower install retinajs --save```  
* Add ```retina.js``` to index.html
  * Not sure ```retina.js``` is working! Irrelevant though as this project  
    won't be using ``retina.js```
  
* **Responsive Images:**  
  * Problem to solve: How can browser fetch the right resolution image for the    
  right combination of browser/viewport size and screen resolution ?
  * Solution: use ```srcset``` attribute in ```<img>``` tag 
  * Also optionally use "sizes" attribute to adjust image width for different viewport  
  widths
```
<!-- Example usage of srcset attribute -->
 <img src="images/logo.png" alt="Company logo"
           srcset="images/logo.png 208w,
                   images/logo@2x.png 415w,
                   images/logo1000.png 1000w,
                   images/logo2000.png 2000w"
           sizes="(max-width: 500px) 100%, (min-width: 501px) 300px">                   
```
* Download Picturefill polyfill to add browser support for  
  ```<picture> element, srcset and sizes attributes```
```bower install picturefill --save```  
* Tried things with IE11 and didn't seem to work though!
  * Chrome and FF work ok.
* Fix logo size width to 208px as it is in Mockup . Figure out sizing for  
  phones and tablets later.
* Discussed merits of SVG (Scalable Vector Graphics) vs Bitmap images
* Create ```_slideshow.scss``` partial to experiment with media queries to load
  different size images based on viewport width
* Skip installing breakpoint-sass and Compass. The former requires Compass but  
  according to the Compass site, Compass is no longer actively maintained.  
* Add slideshow ```<div>``` to ```index.html```
* Add @media queries for images to _slideshow.scss
* Incorporate ```_slide.scss``` partial into ```_slideshow.scss``` partial and refactor
* Delete ```_slide.scss``` partial  
* Style article author images
  * Use inline style for author photo since photo will be different for each author
  * Update _snippet.scss with author photo styling
* Done with styling images 


### 15 Styling Slideshows and Carousels
* Start work on sliders/slideshows
* Start on Main slideshow first
  * Download flexslider ```bower install flexslider --save```
* Update ```_slideshow.scss``` with styling
* Horizontally center slideshow content ```slideshow-slide``` with padding and width  
* Tried to use Flexbox to align slide content but it didn't work as in video
* Review ```https://vanseodesign.com/css/vertical-centering/```
* Update ```_slideshow.scss``` to vertically align slideshow content using 
  ```transform``` method
  * My CSS deviates slightly from video due to difference in FlexSlider version  
    being used.
* Style slideshow previous and next buttons
  * Looks like arrow-next.svg and arrow-prev.svg files are not in the resource pack
  * Video goes through the procedure of creating the arrows with Adobe Illustrator  
    and uploading them to Fontastic 
  * My CSS deviates from video since my arrow icons are different
* Best way to over-ride third party library styling is to use the third party
  settings and add our selector (most likely class)  to make ours more specific. 
* Start work on "Written By Our Team" slideshow   
  * Use **_continuous floats_** to vertically arrange author photo and author post
  * Don't forget about about the three tricks of a _block formatting context_ **_(BFC)_**
    * Containing floats
    * Preventing margin collapse
    * Preventing document flow from wrapping around a floated element
* Discuss how sometimes it's better to specify units in pixels (width/height/margin/padding) 
  when trying to maintain a specific layout (i.e two-column etc) rather than
  having things scale with font-size   
* Start styling snippet/article carousel
  * Replaced flexslider version 2.7.1 with version 2.5.0
```
    bower uninstall flexslider
    bower install flexslider#2.5.0 --save
```  
  * I thought version 2.7.1 had a bug cause the navigation buttons would  
    move back and forth when I hovered over the content. There was no bug 
    though, I just had to position the navigation buttons the same when hovering
    over the content and not hovering.
  * Add FontAwesome arrow icon to replace default FlexSlider arrow icons
  * Add styles to _snippet_slideshow.scss 
  * Carousel doesn't work perfectly though and appears to be not fully responsive.
  
### 16. How to Work With Responsive Layouts
* Responsive design helps to solve the problem of how a website is supposed to  
  look on very small screens while still maintaining some design integrity.
* For **_Mobile first approach_** use ```min-width``` in media queries instead of ``max-width``  
  With this strategy, base styles apply to small screens and when screen size   
  gets larger, styles specified by _media queries_ are applied.
* Begin working on layout - match mockup and make responsive
* Since we were given a desktop mockup we initially used **_desktop first styling_** instead of  
  **_mobile first styling_**
* Tackle footer first 
  * Group components with div so we can control their layout
  * Update ```_site_footer.scss``` with media queries
  * Write media queries using ```min-width``` to follow mobile first design principles
* Fix "Qualities" section layout to look like mockup and make responsive
  * Note that ```display: inline-block``` was used instead of floats to get icon boxes  
    to layout horizontally
  * Adjust layout so that padding gets smaller as viewport gets smaller.  
    This is a design decision that is not apparent in the mockup.
  * Add ```_layout.scss``` partial with placeholder selector - add selector to  
    ```stacked_hero_section``` class
    * Main purpose of ```_layout.scss``` is to limit stacked hero section width to 960px
  * **_Looks like nesting @media queries inside selector now works. Breakpoint Sass  
    extension and Compass isn't needed._** 
  * Use ```max-width``` property for an element's width to prevent it from 
    getting bigger than it's container. 
  * The techniques used to style the icon boxes can be used to style any grid of  
    elements. 
* Make "Written By Our Team" carousel section responsive
  * Strategy here is to see at what width the carousel previous and next
    navigation buttons get cutoff and adjust styles via media queries
  * In general for small screen sizes you want to change any fixed-width elements or  
    containers to ```width: auto```  
  *  Update ```_stacked_hero_section.scss``` and ```_snippet_slideshow.scss```   
     with media queries 
  * Fix horizontal scrolling caused by slideshow and snippet slideshow by setting  
    ```overflow: hidden, width: auto, box-sizing: border-box``` in container
  * Hide prev/next button when viewport is less than ```$breakpoint_smallest_snippet``` 
  
* Adjust contact form to make it responsive
  * Update _contact_form.scss with media query to adjust width
  * Wrap radio buttons in ```<div>``` and set to ```display: inline-block``` so the buttons  
    wrap properly on small screens
* Work on site header to make it responsive - handle top and bottom portions seperately  
  * Set top header width to 960px
    * Wrap Account icon and search form in ```<div>``` so we can style them together
    * Use floats to position Company logo, Account link and Search form
    * Use ```vertical-align``` and ``line-height``` to vertically center elements
  * Set bottom header width to 960px after adding wrapper div
    * Use ```float: left``` for nav bar and ```float: right``` for phone link
    
### 17. How to Work With Responsive Layouts (Part 2) 
* Continue working on site header
* Sidebar
  * Discuss Chrome tools mobile device simulation features
* Fix main slideshow for small screen sizes  
* Sidebar
  * Discuss how z-index property works  
* Fix issue of site search form select fields being hidden by site
  navigation header (at small screen widths)
  * Solution: Add ```{dropdownParent: "body"}``` to ```selectize``` JS function  
* Per the mockup add add a curved drop shadow at the bottom of main slideshow
  * The following css will accomplish the above
  ```
  .slideshow-wrapper {
    position: relative; }
    .slideshow-wrapper:after {
      content: "";
      position: absolute;
      left: 5%;
      right: 5%;
      height: 50%;
      bottom: 0;
      box-shadow: 0 0 30px rgba(0, 0, 0, 0.6);
      border-radius: 30%;
      z-index: -1; }
   ```   
  * We add a dummy element (using css) to ```.slideshow-wrapper div``` and add a  
    box shadow to it
* View site on different browsers and make minor tweaks as necessary    

### 18. Optimizing Front End Performance and Adding Automation
* Access site running on MacBook since it looks like ```http://fed.buildamodule.com/original```
  is not working anymore
#### Performance
* By default a browser will cache every file that it downloads unless that file  
  specifically states that it should not be cached
* The critical phase for maintaining interest in a website is in the initial page load  
* Two main ways to speed up a site (sans caching)
  * Reduce number of files browser has to download
  * Reduce size of files
* How to reduce number of files browser has to download  
  * Merge/concatenate all CSS files into one file
  * Merge/concatenate all JS files into one file
* How to reduce size of files
  * Minify CSS and JS files  
#### Automation  
* We'll use ``Grunt`` instead of ```Gulp``` for automation. Gulp allows for parallel  
  processing so it's potentially faster than Gulp but Grunt is at this point  
  easier to understand is more popular
* Installing and running ```Grunt```  
  * See https://gruntjs.com/getting-started for full documentation and
    * https://github.com/gruntjs/grunt-contrib-concat
    * https://github.com/gruntjs/grunt-contrib-uglify
    * https://github.com/gruntjs/grunt-contrib-cssmin
    * https://github.com/klei/grunt-injector  
    
**_Grunt commands and plugins for this project_**
```
// Install grunt-cli
npm install -g grunt-cli  
// Create package.json file with PhpStorm  

// Install grunt  
npm install grunt --save-dev  

// Install grunt file concatenation plugin
npm install grunt-contrib-concat --save-dev

// Create Gruntfile.js using grunt and grunt plugin documentation

// Run grunt tasks
grunt

// Install grunt JS minification plugin
npm install grunt-contrib-uglify --save-dev

// Update Gruntfile.js with uglify config and task

// Install grunt CSS minification plugin
npm install grunt-contrib-cssmin --save-dev

// Update Gruntfile.js with cssmin config and task

// Install grunt injector plugin
npm install grunt-injector --save-dev

// NOTE: Run npm audit to see if there are any package vulnerabilities
// Update Gruntfile.js with injector config and task

// Update Gruntfile.js with default (dev) and prod versions

// Run default/dev version
grunt --verbose

// Run prod version
grunt prod --verbose

```
* Discussed the use of ```grunt auto-prefixer``` plugin https://github.com/nDmitry/grunt-autoprefixer  
  but this plugin has been deprecated in favor of the ```grunt PostCSS``` plugin  
  https://github.com/nDmitry/grunt-postcss
* _Didn't use either plugin for this project_    