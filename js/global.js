$(function () {
  $('select').selectize({dropdownParent: "body"});
});

$(window).on('load', function () {
  $('.slideshow').flexslider({
    animation: 'fade', pauseOnHover: true, slideshow: false
  });

  $('.snippet_slideshow').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 515,
    itemMargin: 5,
    controlNav: false,
    slideshow: false
  });

});